#!/bin/bash
export MAIN_ACCOUNT=nearlend-official.testnet
export NEAR_ENV=testnet
export OWNER_ID=$MAIN_ACCOUNT
export ORACLE_ID=price-oracle.$MAIN_ACCOUNT
export ACCOUNT_ID=$MAIN_ACCOUNT
export CONTRACT_ID=nearlend.$MAIN_ACCOUNT
export BOOSTER_TOKEN_ID=ref.fakes.testnet
export WETH_TOKEN_ID=weth.fakes.testnet
export DAI_TOKEN_ID=dai.fakes.testnet
export USDT_TOKEN_ID=usdt.fakes.testnet
export USDC_TOKEN_ID=usdc.testnet
export AURORAX_TOKEN_ID=aurorax.$OWNER_ID
export NEL_TOKEN_ID=nearlendtest.testnet
export WNEAR_TOKEN_ID=wrap.testnet
export ASAC_ID=asac.testnet
export NEAR_NAUT_NFT_ID=nearnautnft.testnet
export SECRET_ID=secretskelliessociety.testnet
export GRIMS_ID=grimms.secretskelliessociety.testnet
export UN_DEAD_ID=undead.secretskelliessociety.testnet
export UGLYAPES_ID=uglyapes.neartopia.testnet
export ONE_YOCTO=0.000000000000000000000001
export GAS=200000000000000
export DECIMAL_18=000000000000000000
export DECIMAL_24=000000000000000000000000

echo "###################### Build Contract #####################"
./build.sh

echo "################### DELETE ACCOUNT ###################"
near delete $ORACLE_ID $ACCOUNT_ID

echo "################### CREATE CONTRACT ID ###################"
near create-account $ORACLE_ID --masterAccount $ACCOUNT_ID --initialBalance 10

echo "################### DEPLOY CONTRACT ###################"
near deploy $ORACLE_ID --accountId $ACCOUNT_ID --wasmFile ./res/price_oracle.wasm

#echo "################### INIT CONTRACT ###################"
near call $ORACLE_ID  new '{"recency_duration_sec": 90, "recency_duration_sec_nft": 3600, "owner_id": "'$ACCOUNT_ID'", "near_claim_amount": "20'$DECIMAL_24'"}'  --accountId=$ORACLE_ID
#echo "################### UPDATE CLAIM AMOUT ###################"
#near call $ORACLE_ID update_near_claim_amount '{"near_claim_amount": "10'$DECIMAL_24'"}' --accountId $ACCOUNT_ID --depositYocto 1
#near call $ORACLE_ID set_recency_duration_sec '{"set_recency_duration_sec": 3600}' --accountId $ACCOUNT_ID --depositYocto 1

near call $ORACLE_ID add_oracle '{"account_id": "priceoracle.nhtera.testnet"}' --accountId $ACCOUNT_ID --depositYocto 1
near call $ORACLE_ID add_oracle '{"account_id": "bot-ft-1.nearlend-official.testnet"}' --accountId $ACCOUNT_ID --depositYocto 1
near call $ORACLE_ID add_oracle '{"account_id": "bot-nft-1.nearlend-official.testnet"}' --accountId $ACCOUNT_ID --depositYocto 1
near call $ORACLE_ID remove_oracle '{"account_id": "nft.nearlend-official.testnet"}' --accountId $ACCOUNT_ID --depositYocto 1
#near call $ORACLE_ID remove_oracle '{"account_id": "priceoracle.nhtera.testnet"}' --accountId $ACCOUNT_ID --depositYocto 1

#near call $ORACLE_ID add_oracle '{"account_id": "bot1.duonghb3.testnet"}' --accountId $ACCOUNT_ID --depositYocto 1
#near call $ORACLE_ID add_oracle '{"account_id": "bot2.duonghb3.testnet"}' --accountId $ACCOUNT_ID --depositYocto 1


echo "################### ADD ASSET ###################"
near call $ORACLE_ID add_asset '{"asset_id": "wrap.testnet"}' --accountId $ACCOUNT_ID --depositYocto 1
near call $ORACLE_ID add_asset '{"asset_id": "usdt.fakes.testnet"}' --accountId $ACCOUNT_ID --depositYocto 1
near call $ORACLE_ID add_asset '{"asset_id": "usdc.fakes.testnet"}' --accountId $ACCOUNT_ID --depositYocto 1
near call $ORACLE_ID add_asset '{"asset_id": "dai.fakes.testnet"}' --accountId $ACCOUNT_ID --depositYocto 1
near call $ORACLE_ID add_asset '{"asset_id": "wbtc.fakes.testnet"}' --accountId $ACCOUNT_ID --depositYocto 1
near call $ORACLE_ID add_asset '{"asset_id": "aurora.fakes.testnet"}' --accountId $ACCOUNT_ID --depositYocto 1
near call $ORACLE_ID add_asset '{"asset_id": "usdn.testnet"}' --accountId $ACCOUNT_ID --depositYocto 1
near call $ORACLE_ID add_asset '{"asset_id": "aurora"}' --accountId $ACCOUNT_ID --depositYocto 1
near call $ORACLE_ID add_asset '{"asset_id": "weth.fakes.testnet"}' --accountId $ACCOUNT_ID --depositYocto 1
near call $ORACLE_ID add_asset '{"asset_id": "'$ASAC_ID'"}' --accountId $ACCOUNT_ID --depositYocto 1
near call $ORACLE_ID add_asset '{"asset_id": "'$NEAR_NAUT_NFT_ID'"}' --accountId $ACCOUNT_ID --depositYocto 1
near call $ORACLE_ID add_asset '{"asset_id": "'$SECRET_ID'"}' --accountId $ACCOUNT_ID --depositYocto 1
near call $ORACLE_ID add_asset '{"asset_id": "'$GRIMS_ID'"}' --accountId $ACCOUNT_ID --depositYocto 1
near call $ORACLE_ID add_asset '{"asset_id": "'$UN_DEAD_ID'"}' --accountId $ACCOUNT_ID --depositYocto 1
near call $ORACLE_ID add_asset '{"asset_id": "'$UGLYAPES_ID'"}' --accountId $ACCOUNT_ID --depositYocto 1
near call $ORACLE_ID add_asset '{"asset_id": "nft.nearlend-official.testnet"}' --accountId $ACCOUNT_ID --depositYocto 1

near view $ORACLE_ID get_oracles '{"from_index": 0, "limit": 10}'
near view $ORACLE_ID get_assets '{"from_index": 0, "limit": 20}'
near view $ORACLE_ID get_price_data '{"asset_ids": ["'$DAI_TOKEN_ID'", "'$USDT_TOKEN_ID'"]}'
